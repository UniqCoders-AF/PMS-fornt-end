angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
        $stateProvider
            .state('app.icons', {
                url: "/icons",
                abstract: true,
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'Icons'
                }
            })
            .state('app.icons.fontawesome', {
                url: '/font-awesome',
                templateUrl: 'views/icons/font-awesome.html',
                ncyBreadcrumb: {
                    label: 'Font Awesome'
                }
            })
            .state('app.icons.simplelineicons', {
                url: '/simple-line-icons',
                templateUrl: 'views/icons/simple-line-icons.html',
                ncyBreadcrumb: {
                    label: 'Simple Line Icons'
                }
            })
            // New code---
            // -->> Stocks routes set -------
            .state('app.stocks', {
                url: "/stocks",
                abstract: true,
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'Stocks'
                }
            })

            .state('app.stocks.available-stocks', {
                url: '/stocks/avaiable-stocks',
                templateUrl: 'views/stocks/available-stocks.html',
                ncyBreadcrumb: {
                    label: 'Available Stocks'
                }
            })

            .state('app.stocks.view-drug-requests', {
                url: '/stocks/drug-requests',
                templateUrl: 'views/stocks/view-drug-requests.html',
                ncyBreadcrumb: {
                    label: 'view Drug Requests'
                }
            })
            .state('app.stocks.place-orders', {
                url: '/stocks/orders',
                templateUrl: 'views/stocks/place-orders.html',
                ncyBreadcrumb: {
                    label: 'place-orders'
                }
            })
            .state('app.stocks.add-new-batch', {
                url: '/stocks/addBatch',
                templateUrl: 'views/stocks/add-new-batch.html',
                ncyBreadcrumb: {
                    label: 'Add Batch'
                }
            })
            .state('app.stocks.reports', {
                url: '/stocks',
                templateUrl: 'views/stocks/reports.html',
                ncyBreadcrumb: {
                    label: 'reports'
                }
            })
            // <<-- Stocks routes set -------


            // ->> Drugs routes set -----------

            .state('app.drugs', {
                url: "/drugs",
                abstract: true,
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'drugs'
                }
            })

            .state('app.drugs.add-drugs', {
                url: '/drugs',
                templateUrl: 'views/drugs/drug-handling.html',
                ncyBreadcrumb: {
                    label: 'add-drugs'
                }
            })
            .state('app.drugs.drug-information', {
                url: '/drugs',
                templateUrl: 'views/drugs/drug-information.html',
                ncyBreadcrumb: {
                    label: 'add-drugs'
                }
            })
            .state('app.drugs.add-drug-category', {
                url: '/drugs',
                templateUrl: 'views/drugs/DrugCategoryType.html',
                ncyBreadcrumb: {
                    label: 'add-drug-category'
                }
            })
            .state('app.drugs.category-Information', {
                url: '/drugs',
                templateUrl: 'views/drugs/category-Information.html',
                ncyBreadcrumb: {
                    label: 'category-Information'
                }
            })
            .state('app.drugs.reports', {
                url: '/drugs',
                templateUrl: 'views/drugs/reports.html',
                ncyBreadcrumb: {
                    label: 'reports'
                }
            })

            // <<-- Drugs routes set -----------

            // ->> Billing routes

            .state('app.bills', {
                url: "/bills",
                abstract: true,
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'bills'
                }
            })

            .state('app.bills.drug-dispense', {
            url: '/bills',
            templateUrl: 'views/bills/drug-dispense.html',
            ncyBreadcrumb: {
                label: 'drug-dispense'
            }
            })

            .state('app.bills.drug-availability', {
                url: '/bills',
                templateUrl: 'views/bills/drug-availability.html',
                ncyBreadcrumb: {
                    label: 'drug-availability'
                }
            })

            .state('app.bills.bills', {
                url: '/bills',
                templateUrl: 'views/bills/bills.html',
                ncyBreadcrumb: {
                    label: 'bills'
                }
            })

            .state('app.bills.reports', {
                url: '/bills',
                templateUrl: 'views/bills/reports.html',
                ncyBreadcrumb: {
                    label: 'reports'
                }
            })

        // <<-- Billing routes

        // <<--Suppliers routes

            .state('app.suppliers', {
                url: "/suppliers",
                abstract: true,
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'suppliers'
                }
            })

            .state('app.suppliers.add-suppliers', {
                url: '/suppliers',
                templateUrl: 'views/suppliers/add-suppliers.html',
                ncyBreadcrumb: {
                    label: 'add-suppliers'
                }
            })

            .state('app.suppliers.supplier-Information', {
                url: '/suppliers',
                templateUrl: 'views/suppliers/supplier-Information.html',
                ncyBreadcrumb: {
                    label: 'supplier-Information'
                }
            })

            .state('app.suppliers.reports', {
                url: '/suppliers',
                templateUrl: 'views/suppliers/reports.html',
                ncyBreadcrumb: {
                    label: 'reports'
                }
            })

            .state('app.prescriptions', {
                url: "/prescriptions",
                abstract: true,
                template: '<ui-view></ui-view>',
                ncyBreadcrumb: {
                    label: 'prescriptions'
                }
            })

            .state('app.prescriptions.add', {
                url: '/prescriptions/add',
                templateUrl: 'views/prescriptions/prescriptions.html',
                ncyBreadcrumb: {
                    label: 'Add'
                }
            })
            .state('app.prescriptions.view', {
                url: '/prescriptions/view',
                templateUrl: 'views/prescriptions/viewPrescriptions.html',
                ncyBreadcrumb: {
                    label: 'View Prescriptions'
                }
            })


        // <<-- Suppliers routes


            // ------------------------------------------------------------------
            /*
            .state('app.forms', {
                url: '/forms',
                templateUrl: 'views/forms.html',
                ncyBreadcrumb: {
                    label: 'Forms'
                },
                resolve: {
                    loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                serie: true,
                                files: ['js/libs/moment.min.js']
                            },
                            {
                                serie: true,
                                files: ['js/libs/daterangepicker.min.js', 'js/libs/angular-daterangepicker.min.js']
                            },
                            {
                                files: ['js/libs/mask.min.js']
                            },
                            {
                                files: ['js/libs/select.min.js']
                            }
                        ]);
                    }],
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load({
                            files: ['js/controllers/forms.js']
                        });
                    }]
                }
            })

            .state('app.widgets', {
                url: '/widgets',
                templateUrl: 'views/widgets.html',
                ncyBreadcrumb: {
                    label: 'Widgets'
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load controllers
                        return $ocLazyLoad.load({
                            files: ['js/controllers/widgets.js']
                        });
                    }]
                }
            })
            .state('app.charts', {
                url: '/charts',
                templateUrl: 'views/charts.html',
                ncyBreadcrumb: {
                    label: 'Charts'
                },
                resolve: {
                    // Plugins loaded before
                    // loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load([
                    //         {
                    //             serial: true,
                    //             files: ['js/libs/Chart.min.js', 'js/libs/angular-chart.min.js']
                    //         }
                    //     ]);
                    // }],
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load({
                            files: ['js/controllers/charts.js']
                        });
                    }]
                }
            })
            */

    }]);
