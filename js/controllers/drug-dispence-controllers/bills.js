/**
 * Created by Lasitha on 7/2/2017.
 */

'use strict';
angular
    .module('app')
    .controller('billCtrl',billCtrl );

billCtrl.$inject=['$http','$scope','urlService'];

function billCtrl($http, $scope, urlService) {

    $scope.abc = function () {
        console.log('I am inside Bill controller');
    };
    $scope.abc();

    this.$http = $http;

    let vm = this;

    $scope.getBills = ()=>{
        $http.get(`${urlService.baseUrl}bills/getAll`).then(result=>{
            vm.billData = result.data;
            $scope.bills = vm.billData;
            console.log($scope.bills);

        }).catch(err=>{
            throw err;
        });
    };
    $scope.getBills();

    $scope.getBillById = (billParam)=>{
        $http.get(`${urlService.baseUrl}bills/id/${billParam._id}`).then(result=>{
            vm.billData = result.data;
            $scope.bill = vm.billData;
            console.log($scope.bill);

        }).catch(err=>{
            throw err;
        });
    };
    $scope.getBills();


}
