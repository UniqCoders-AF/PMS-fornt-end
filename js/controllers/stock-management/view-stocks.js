'use strict'
/*console.log("I am from the stocks js");*/
angular
    .module('app')
    .controller('StockCtrl', StockCtrl);

StockCtrl.$inject = ['$http', '$scope','urlService'];
function StockCtrl($http, $scope,urlService) {

   console.log("I am inside a Stock Controller");
    this.$http = $http;

    var vm = this;


    $scope.getAllDrugs = ()=>{
        $http.get(`${urlService.baseUrl}drugs/getAll`).then(function (result) {
            vm.drugData = result.data;
            $scope.drugs = vm.drugData;
            console.log($scope.drugs);
        });
    };
    $scope.getAllDrugs();

    $scope.sendDrugReqFrom = (val)=>{

        let requestMessage = `Enter the request amount for ${val.name} `;
        let drugName = val.name;

        swal({
                title: "Send Drug Request",
                text: requestMessage,
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Enter the amount you need to request"
            },
            function(inputValue){
                if (inputValue === false) return false;

                if (inputValue === "") {
                    swal.showInputError("You need to enter the amount!");
                    return false
                }

                $scope.sendDrugReqs ={
                    drugName:val.name,
                    requestedQty:inputValue,
                    avaiableQty:val.quantity,
                    status:"Pending"
                }
                console.log($scope.sendDrugReqs);
                $http.post(`${urlService.baseUrl}drugRequests/add`,$scope.sendDrugReqs).then(result=>{
                    swal("Success!", "You have requested " + inputValue + " items from " + drugName, "success");
                });


            });
    }



}
