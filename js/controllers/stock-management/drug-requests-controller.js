/**
 * Created by Dhanuka Perera on 7/2/2017.
 */
"use strict";
angular
    .module('app')
    .controller('DrugRequestCtrl', DrugRequestCtrl);

DrugRequestCtrl.$inject = ['$http', '$scope', 'urlService'];

function DrugRequestCtrl($http,$scope,urlService) {

    console.log("I am inside drug request controller");
    this.$http = $http;

    let vm = this;
    $scope.getAllDrugRequests = ()=>{
        $http.get(`${urlService.baseUrl}drugRequests/getAll`).then(result=>{
            vm.drugRequestData = result.data;
            $scope.drugRequests = vm.drugRequestData;
            console.log($scope.drugRequests);
        })
    }
    $scope.getAllDrugRequests();

    $scope.approveRequests = (val)=>{

        let msg= `Are you sure you want to approve requested ${val.requestedQty} for ${val.drugName}`;

        swal({
                title: "Are you sure?",
                text: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Approve",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {

                    $scope.approveStatus ={
                        status : "Approved"
                    }
                    console.log($scope.approveStatus);
                    $http.put(`${urlService.baseUrl}drugRequests/approve/${val._id}`,$scope.approveStatus).then(resut=>{
                        swal("Approved!", "Drug Request has been approved.", "success");
                    });

                } else {
                    swal("Cancelled", "Drug Request Canceled", "error");
                }
            });
    }
}