/**
 * Created by Dhanuka Perera on 6/30/2017.
 */
"use strict";
angular
    .module('app')
    .controller('AddBatchCtrl', AddBatchCtrl);

AddBatchCtrl.$inject = ['$http', '$scope', 'urlService'];

function AddBatchCtrl($http, $scope, urlService) {

    console.log("I am inside add batch controller");
    console.log(urlService.baseUrl);

    this.$http = $http;

    let vm = this;

    $scope.getDrugCategories = () => {
        $http.get(`${urlService.baseUrl}drugs/categories/getAll`).then(result => {
            vm.drugCategoriesData = result.data;
            $scope.drugCategories = vm.drugCategoriesData;
            console.log($scope.drugCategories);
        }).catch(err => {
            throw err;
        });
    };

    $scope.getDrugCategories();

    $scope.getDrugsByCategory = (category) => {
        console.log(category);
        $http.get(`${urlService.baseUrl}drugs/getDrugsByCategory/${category._id}`).then(result => {
            vm.drugNameData = result.data;
            $scope.drugNames = vm.drugNameData;
            console.log($scope.drugNames);
        }).catch(err => {
            throw err;
        });
    };

    $scope.genarateBatchNo = () => {
        $scope.batchNumber = Math.round(new Date().getTime() / 1000);
    };

    $scope.getBatchType = () => {
        $http.get(`${urlService.baseUrl}batches/types/getAll`).then(result => {
            vm.batchTypeData = result.data;
            $scope.batchTypes = vm.batchTypeData;
            console.log($scope.batchTypes);
        }).catch(err => {
            throw err;
        });
    };

    $scope.getBatchType();

    // Get contents
    $scope.contentVisibility = false;
    $scope.populateContent = () => {
        $scope.contentVisibility = true;
        $http.get(`${urlService.baseUrl}batches/contents/getAll`).then(result => {
            vm.batchContentData = result.data;
            $scope.batchContents = vm.batchContentData;
            console.log($scope.batchContents);

        }).catch(err => {
            throw err;
        });
    };


    // Filter records
    $scope.contentVisibility = false;
    $scope.displayContentFileds = () => {
        $scope.contentVisibility = true;
        $http.get(`${urlService.baseUrl}batches/contents/getAll`).then(result => {
            vm.batchContentData = result.data;
            $scope.batchContents = vm.batchContentData;
            console.log($scope.batchContents);

        }).catch(err => {
            throw err;
        });
    };

    $scope.populateContentTypes = () => {
        $http.get(`${urlService.baseUrl}batches/contentTypes/getAll`).then(result => {
            vm.batchContentTypesData = result.data;
            $scope.batchContentTypes = vm.batchContentTypesData;
            console.log($scope.batchContentTypes);
        }).catch(err => {
            throw err;
        });
    }


    $scope.contentTypeVisibility = false;
    $scope.numberOfCartoonsVisibility = false;
    $scope.numberOfBottlesPerCartoonVisibility = false;
    $scope.numberOfBottlesVisibility = false;
    $scope.tabletsPerBottleVisibility = false;
    $scope.numberOfTabletsPerBottleVisibility = false;
    $scope.numberOfCartoonsPerBottleVisibility = false;
    $scope.numberOfCardsPerCartoonVisibilty = false;
    $scope.numberOfTabletsPerCardVisibility = false;

    $scope.displayContentTypeFileds = () => {

        if ($scope.batchType.batchType === 'Cartoons') {


            if ($scope.batchContent.batchContent === 'Tablets') {
                //  $scope.contentTypeVisibility = true;
                $scope.populateContentTypes();

                $scope.contentTypeVisibility = true;
                $scope.numberOfCartoonsVisibility = false;
                $scope.numberOfBottlesPerCartoonVisibility = false;
                $scope.numberOfBottlesVisibility = false;
                $scope.tabletsPerBottleVisibility = false;
                $scope.numberOfTabletsPerBottleVisibility = false;
                $scope.numberOfCartoonsPerBottleVisibility = false;
                $scope.numberOfCardsPerCartoonVisibilty = false;
                $scope.numberOfTabletsPerCardVisibility = false;
            } else if ($scope.batchContent.batchContent === 'Liquied') {
                $scope.contentTypeVisibility = false;
                $scope.numberOfCartoonsVisibility = true;
                $scope.numberOfBottlesPerCartoonVisibility = true;
                $scope.numberOfBottlesVisibility = false;
                $scope.tabletsPerBottleVisibility = false;
                $scope.numberOfTabletsPerBottleVisibility = false;
                $scope.numberOfCartoonsPerBottleVisibility = false;
                $scope.numberOfCardsPerCartoonVisibilty = false;
                $scope.numberOfTabletsPerCardVisibility = false;

                $scope.state = "C";
                $scope.calculate();
            }

        } else if ($scope.batchType.batchType === 'Bottles') {
            if ($scope.batchContent.batchContent === 'Tablets') {
                $scope.contentTypeVisibility = false;
                $scope.numberOfCartoonsVisibility = false;
                $scope.numberOfBottlesPerCartoonVisibility = false;
                $scope.numberOfBottlesVisibility = true;
                $scope.tabletsPerBottleVisibility = true;
                $scope.numberOfTabletsPerBottleVisibility = false;
                $scope.numberOfCartoonsPerBottleVisibility = false;
                $scope.numberOfCardsPerCartoonVisibilty = false;
                $scope.numberOfTabletsPerCardVisibility = false;

                $scope.state = "D";
                $scope.calculate();

            } else if ($scope.batchContent.batchContent === 'Liquied') {
                $scope.contentTypeVisibility = false;
                $scope.numberOfCartoonsVisibility = false;
                $scope.numberOfBottlesPerCartoonVisibility = false;
                $scope.numberOfBottlesVisibility = true;
                $scope.tabletsPerBottleVisibility = false;
                $scope.numberOfTabletsPerBottleVisibility = false;
                $scope.numberOfCartoonsPerBottleVisibility = false;
                $scope.numberOfCardsPerCartoonVisibilty = false;
                $scope.numberOfTabletsPerCardVisibility = false;
                $scope.state = "E";
                $scope.calculate();
            }

        }


    };


    $scope.contentTypeFilter = () => {
        console.log($scope.contentType.batchContentType);


        if ($scope.contentType.batchContentType === 'Bottles') {
            $scope.contentTypeVisibility = true;
            $scope.numberOfCartoonsVisibility = true;
            $scope.numberOfBottlesPerCartoonVisibility = true;
            $scope.numberOfBottlesVisibility = false;
            $scope.tabletsPerBottleVisibility = false;
            $scope.numberOfTabletsPerBottleVisibility = true;
            $scope.numberOfCartoonsPerBottleVisibility = false;
            $scope.numberOfCardsPerCartoonVisibilty = false;
            $scope.numberOfTabletsPerCardVisibility = false;

            $scope.state = "A";
            $scope.calculate();
            // $scope.quantity =$scope.numberOfCartoons*$scope.numberOfCartoonsPerBottle*$scope.numberOfTabletsPerBottle;
        } else if ($scope.contentType.batchContentType === 'Cards') {
            $scope.contentTypeVisibility = true;
            $scope.numberOfCartoonsVisibility = true;
            $scope.numberOfBottlesPerCartoonVisibility = false;
            $scope.numberOfBottlesVisibility = false;
            $scope.tabletsPerBottleVisibility = false;
            $scope.numberOfTabletsPerBottleVisibility = false;
            $scope.numberOfCartoonsPerBottleVisibility = false;
            $scope.numberOfCardsPerCartoonVisibilty = true;
            $scope.numberOfTabletsPerCardVisibility = true;
            $scope.state = "B";
            $scope.calculate();
        }

        //if
    };

    $scope.quantity = 1;
    $scope.numberOfCartoons = 1;
    $scope.numberOfCartoonsPerBottle = 1;
    $scope.numberOfTabletsPerBottle = 1;
    $scope.numberOfCardsPerCartoon = 1;
    $scope.numberOfTabletsPerCartoon = 1;
    $scope.numberOfBottles = 1;
    $scope.tabletsPerBottle = 1;
    $scope.calculate = () => {
        console.log($scope.state);

        if ($scope.state === "A") {
            $scope.quantity = $scope.numberOfCartoons * $scope.numberOfCartoonsPerBottle * $scope.numberOfTabletsPerBottle;
        } else if ($scope.state === "B") {
            $scope.quantity = $scope.numberOfCartoons * $scope.numberOfCardsPerCartoon * $scope.numberOfTabletsPerCartoon;
        } else if ($scope.state === "C") {
            $scope.quantity = $scope.numberOfCartoons * $scope.numberOfCartoonsPerBottle;
        } else if ($scope.state === "D") {
            $scope.quantity = $scope.numberOfBottles * $scope.tabletsPerBottle;
        } else if ($scope.state === "E") {
            $scope.quantity = $scope.numberOfBottles;
        }
    }

    $scope.validateManifactureDate = () => {
        let d1 = new Date($scope.manfacureDate).getTime();
        let d2 = new Date().getTime();

        if (d2 < d1) {
            $scope.showManiDateErr = true;
        } else {
            $scope.showManiDateErr = false;
        }
    }

    $scope.validateExpDate = () => {
        let d1 = new Date($scope.expDate).getTime();
        let d2 = new Date().getTime();

        if (d2 > d1) {
            $scope.showExpDateErr = true;
        } else {
            $scope.showExpDateErr = false;
        }
    };

    $scope.getCurrentQty = (drugId) => {
        let drugQty = 0;
        $http.get(`${urlService.baseUrl}drugs/getDrugQty/${drugId}`).then(result => {
            vm.drugQtyData = result.data;
            drugQty = vm.drugQtyData;
            console.log($scope.drugQty);

        }).catch(err => {
            throw err;
        });
        return drugQty;
    };

    $scope.geAllBatches = () => {
        $http.get(`${urlService.baseUrl}batches/getAllBatches`).then(result => {
            vm.batchData = result.data;
            $scope.batches = vm.batchData;
            console.log($scope.batches);

        }).catch(err => {
            throw err;
        });
    }

    $scope.geAllBatches();

    $scope.addBatch = () => {



        if($scope.drugNames === undefined ||
            $scope.quantity=== undefined ||
            $scope.drugName ===undefined ||
            $scope.batchNumber  ===undefined ||
            $scope.manfacureDate ===undefined ||
            $scope.expDate===undefined ||
            $scope.drugCategory === undefined
        ) {
            swal("Error!", "Please fill all the Fields", "error");
        }else{





        $scope.quantity = $scope.quantity + $scope.drugName.quantity;
        $scope.batch = {
            drugname: $scope.drugName.name,
            batchNumber: $scope.batchNumber,
            manufacureDate: $scope.manfacureDate,
            expireDate: $scope.expDate,
            quantity: $scope.quantity
        }

        $http.post(`${urlService.baseUrl}batches/addBatch`, $scope.batch).then(result => {
            console.log(result);
            $scope.batches.push($scope.batch);
            $scope.geAllBatches();
        });

        $scope.updateQty = {
            quantity: $scope.quantity
        }

        console.log($scope.updateQty);
        $http.put(`${urlService.baseUrl}drugs/updateDrugQty/${$scope._id}`, $scope.updateQty).then(result => {
            console.log(result);
        });
        // $scope.geAllBatches();


        console.log($scope.batch);
        console.log($scope.batches);
        //swal("New Batch Added Successfully !");
        swal("Success!", "New Batch Added Successfully !", "success")
        $scope.geAllBatches();
        $scope.clear();
        }
    }

    $scope.clear = () => {
       /* console.log($scope.drugNames === undefined);
        console.log($scope.drugCategory);*/



        $scope.quantity = 1;
        $scope.numberOfCartoons = 1;
        $scope.numberOfCartoonsPerBottle = 1;
        $scope.numberOfTabletsPerBottle = 1;
        $scope.numberOfCardsPerCartoon = 1;
        $scope.numberOfTabletsPerCartoon = 1;
        $scope.numberOfBottles = 1;
        $scope.tabletsPerBottle = 1;
        $scope.drugName = "";
        $scope.batchNumber = "";
        $scope.manfacureDate = "";
        $scope.expDate = "";
        $scope.quantity = 1;
        $scope.drugCategory ="";


    }


};